'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
////

var isin = {};

isin.node = function () {
  return typeof module !== 'undefined';
};

isin.browser = function () {
  return typeof window !== 'undefined';
};

var is = {};

is.function = function (value) {
  return typeof value === 'function';
};

is.undefined = function (value) {
  return value === undefined;
};

is.array = function (value) {
  return Array.isArray(value);
};

is.object = function (value) {
  return value instanceof Object && !is.array(value);
};

is.atom = function (value) {
  return !(is.array(value) || is.object(value));
};

var cat_indentation = 0;

function cat_get_indentation_array() {
  var result = [];
  for (var i = 0; i < cat_indentation; i++) {
    result.push(" ");
  }
  return result;
}

function cat() {
  var argument_array = [];
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = arguments[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var argument = _step.value;

      argument_array.push(argument);
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  if (isin.node()) {
    console.log.apply(console, cat_get_indentation_array().concat(argument_array));
  } else if (isin.browser()) {
    console.log.apply(console, argument_array);
  } else {
    console.log.apply(console, cat_get_indentation_array().concat(argument_array));
  }
}

function call_with_indentation(fun) {
  if (isin.node()) {
    cat_indentation = 1 + cat_indentation;
    fun.call(this);
    cat_indentation = cat_indentation - 1;
  } else if (isin.browser()) {
    console.group();
    fun.call(this);
    console.groupEnd();
  } else {
    cat_indentation = 1 + cat_indentation;
    fun.call(this);
    cat_indentation = cat_indentation - 1;
  }
}

function incat() {
  var _this = this,
      _arguments = arguments;

  call_with_indentation(function () {
    cat.apply(_this, _arguments);
  });
}

// {
//   cat("k1", "k2", "k3");
//   incat("k1", "k2", "k3");
//   call_with_indentation(() => {
//     call_with_indentation(() => cat("k1", "k2", "k3"));
//     call_with_indentation(() => incat("k1", "k2", "k3"));
//   });

//   cat("k1", "k2", "k3");
//   incat("k1", "k2", "k3");
//   call_with_indentation(() => {
//     call_with_indentation(() => cat("k1", "k2", "k3"));
//     call_with_indentation(() => incat("k1", "k2", "k3"));
//   });
// }

function orz() {
  cat.apply(this, arguments);
  console.assert(false);
}

// {
//   orz("k1", "k2", "k3");
// }

function asr() {
  console.assert.apply(console, arguments);
}

function BUFFER(size, init_value) {
  this.size = size;
  this.array = new Array(size);
  this.array.fill(init_value);
  this.cursor = 0;
}

BUFFER.prototype = {
  set: function set(index, value) {
    this.array[index] = value;
  },
  get: function get(index) {
    return this.array[index];
  },
  add_cursor: function add_cursor(value) {
    this.cursor = this.cursor + value;
  },
  allocate: function allocate(size) {
    var return_address = this.cursor;
    this.add_cursor(size);
    return return_address;
  }
};

function STACK(size, init_value) {
  BUFFER.call(this, size, init_value);
}

STACK.prototype = Object.create(BUFFER.prototype);

STACK.prototype.push = function (value) {
  this.set(this.cursor, value);
  this.add_cursor(1);
};

STACK.prototype.pop = function () {
  this.add_cursor(-1);
  return this.get(this.cursor);
};

STACK.prototype.tos = function () {
  return this.get(this.cursor - 1);
};

STACK.prototype.drop = function () {
  this.add_cursor(-1);
};

STACK.prototype.n_drop = function (n) {
  this.add_cursor(-n);
};

STACK.prototype.is_empty = function () {
  return this.cursor === 0;
};

STACK.prototype.print = function () {
  var index = 0;
  var arg_list = [];
  while (index < this.cursor) {
    arg_list.push(this.get(index));
    index = 1 + index;
  }
  arg_list.unshift('  *', this.cursor, '*  --');
  arg_list.push('--');
  console.log.apply(console, arg_list);
};

// {
//   const stack = new STACK(1024, 0);

//   stack.push(666);
//   console.assert(stack.pop() === 666);

// }

function HASH_TABLE_ENTRY(index) {
  this.index = index;
  this.key = null;
  this.value = null;
  this.orbit_length = 0;
  this.orbiton = 0;
}

HASH_TABLE_ENTRY.prototype = {
  occured: function occured() {
    return this.key !== null;
  },

  used: function used() {
    return this.value !== null;
  },

  no_collision: function no_collision() {
    return this.index === this.orbiton;
  }
};

function HASH_TABLE(size, key_equal, hash) {
  this.size = size;
  this.key_equal = key_equal;
  this.hash = hash;
  this.array = new Array(size);
  this.counter = 0;
  for (var i = 0; i < size; i++) {
    this.array[i] = new HASH_TABLE_ENTRY(i);
  };
}

HASH_TABLE.prototype = {
  insert: function insert(key) {
    // key -> index
    //     -> null -- denotes the hash_table is filled
    var orbit_index = this.hash(key, 0);
    var counter = 0;
    while (true) {
      var index = this.hash(key, counter);
      var entry = this.index_to_entry(index);
      if (!entry.occured()) {
        entry.key = key;
        entry.orbiton = orbit_index;
        var orbit_entry = this.index_to_entry(orbit_index);
        orbit_entry.orbit_length = 1 + counter;
        this.counter = 1 + this.counter;
        return index;
      } else if (this.key_equal(key, entry.key)) {
        return index;
      } else if (counter === this.size) {
        return null;
      } else {
        counter = 1 + counter;
      }
    }
  },

  search: function search(key) {
    // key -> index
    //     -> null -- denotes key not occured
    var counter = 0;
    while (true) {
      var index = this.hash(key, counter);
      var entry = this.index_to_entry(index);
      if (!entry.occured()) {
        return null;
      } else if (this.key_equal(key, entry.key)) {
        return index;
      } else if (counter === this.size) {
        return null;
      } else {
        counter = 1 + counter;
      }
    }
  },

  key_to_index: function key_to_index(key) {
    var index = this.insert(key);
    if (index !== null) {
      return index;
    } else {
      console.log("hash_table is filled");
      throw "hash_table is filled";
    }
  },

  index_to_entry: function index_to_entry(index) {
    return this.array[index];
  },

  key_to_entry: function key_to_entry(key) {
    return index_to_entry(key_to_index(key));
  },

  report_orbit: function report_orbit(index, counter) {
    var entry = this.index_to_entry(index);
    while (true) {
      if (counter >= entry.orbit_length) {
        return;
      } else {
        var key = entry.key;
        var next_index = this.hash(key, counter);
        var next_entry = this.index_to_entry(next_index);
        if (index === next_entry.orbiton) {
          console.log("  - {", next_index, "} ", next_entry.key);
        }
        counter = 1 + counter;
      }
    }
  },

  report: function report() {
    console.log("\n");
    console.log("- hash_table-table report_used");
    var index = 0;
    while (true) {
      var entry = this.index_to_entry(index);
      if (index === this.size) {
        break;
      } else {
        if (entry.occured() && entry.no_collision()) {
          console.log("  - {", index, "} ", entry.key, " # ", entry.orbit_length);
          if (entry.used()) {
            console.log("      ", entry.value);
          }
          this.report_orbit(index, 1);
        }
        index = 1 + index;
      }
    }
    console.log("\n");
    console.log("- used : ", this.counter);
    console.log("- free : ", this.size - this.counter);
  }
};

var argack = new STACK(1024, 0);

var retack = new STACK(1024, 0);

var V = function V(name_string) {
  this.local_variable_jo = name_string;
};

var v = function v(name_string) {
  return new V(name_string);
};

var local_variable_jo_p = function local_variable_jo_p(value) {
  return value instanceof Object && typeof value.local_variable_jo === 'string';
};

var RETACK_POINT = function RETACK_POINT(array) {
  this.cursor = 0;
  this.array = array;
  this.local_variable_map = new Map();
};

RETACK_POINT.prototype = {
  get_current_jo: function get_current_jo() {
    return this.array[this.cursor];
  },
  at_tail_position: function at_tail_position() {
    return this.cursor + 1 === this.array.length;
  },
  next: function next() {
    this.cursor = 1 + this.cursor;
  }
};

var send = { injo: 'send' };
var recur = { injo: 'recur' };

retack.eval_primitive_function = function (jo, retack_point) {
  var count_down = jo.length;
  var arg_list = [];
  while (count_down !== 0) {
    arg_list.push(argack.pop());
    count_down = count_down - 1;
  }
  arg_list.reverse();
  var result = jo.apply(this, arg_list);
  if (result !== undefined) {
    argack.push(result);
  }
};

retack.eval_send = function (jo, retack_point) {
  var message = argack.pop();
  var object = argack.pop();
  if (typeof object[message] === 'function') {
    var arg_length = object[message].length;
    var arg_list = [];
    while (arg_length !== 0) {
      arg_list.push(argack.pop());
      arg_length = arg_length - 1;
    }
    arg_list.reverse();
    var result = object[message].apply(object, arg_list);
    if (result !== undefined) {
      argack.push(result);
    }
  } else {
    argack.push(object[message]);
  }
};

var copy_argack = function copy_argack(length) {
  var result = [];
  for (var i = 0; i < length; i++) {
    result.push(argack.get(argack.cursor - length + i));
  }
  return result;
};

retack.unify_atom = function (source, pattern, local_variable_map) {
  return source === pattern;
};

retack.unify_helper_equal = function (old, source) {
  // note the order of source & old here
  // the second argument can contain V
  return retack.unify(source, old, new Map());
};

retack.unify_local_variable_jo = function (source, pattern, local_variable_map) {
  if (local_variable_map.has(pattern.local_variable_jo)) {
    var old = local_variable_map.get(pattern.local_variable_jo);
    if (retack.unify_helper_equal(old, source)) {
      return true;
    } else {
      return false;
    }
  } else {
    local_variable_map.set(pattern.local_variable_jo, source);
    return true;
  }
};

retack.unify_array = function (source, pattern, local_variable_map) {
  var length = source.length;
  for (var i = 0; i < length; i++) {
    if (retack.unify(source[i], pattern[i], local_variable_map)) {
      // nothing
    } else {
        return false;
      }
  }
  return true;
};

retack.unify_object = function (source, pattern, local_variable_map) {
  for (var field in Object.keys(pattern)) {
    if (retack.unify(source[field], pattern[field], local_variable_map)) {
      // nothing
    } else {
        return false;
      }
    return true;
  }
};

retack.unify = function (source, pattern, local_variable_map) {
  if (local_variable_jo_p(pattern)) {
    return retack.unify_local_variable_jo(source, pattern, local_variable_map);
  } else if (is.atom(pattern)) {
    return retack.unify_atom(source, pattern, local_variable_map);
  } else if (is.array(pattern)) {
    return retack.unify_array(source, pattern, local_variable_map);
  } else if (is.object(pattern)) {
    return retack.unify_object(source, pattern, local_variable_map);
  } else {
    // ><><><
    // better error handing
    console.log('- retack.unify');
    console.log('  type unknown pattern : ', pattern);
  }
};

retack.eval_local_variable = function (name_string, local_variable_map) {
  var result = local_variable_map.get(name_string);
  if (result === undefined) {
    // ><><><
    // better error handling
    console.log('- in retack.eval_local_variable');
    console.log('  meet undefined name : ', name_string);
  } else {
    argack.push(result);
  }
};

retack.eval_majo = function (sequent_list, retack_point) {
  var length = sequent_list.length;
  for (var i = 0; i + 1 < length; i = i + 2) {
    var antecedent = sequent_list[i];
    var succedent = sequent_list[i + 1];
    var new_retack_point = new RETACK_POINT(succedent);
    retack.push(new_retack_point);
    var arg_array = copy_argack(antecedent.length);
    var success = retack.unify(arg_array, antecedent, new_retack_point.local_variable_map);
    if (success) {
      argack.n_drop(antecedent.length);
      return;
    } else {
      retack.pop();
    }
  }
};

retack.eval_dispatch = function (jo, retack_point) {
  if (is.function(jo)) {
    retack.eval_primitive_function(jo, retack_point);
  } else if (is.undefined(jo)) {} else if (jo === send) {
    retack.eval_send(jo, retack_point);
  } else if (jo === recur) {
    retack.push(new RETACK_POINT(retack_point.array));
  } else if (is.array(jo.jojo)) {
    retack.push(new RETACK_POINT(jo.jojo));
  } else if (is.array(jo.majo)) {
    retack.eval_majo(jo.majo, retack_point);
  } else if (local_variable_jo_p(jo)) {
    retack.eval_local_variable(jo.local_variable_jo, retack_point.local_variable_map);
  } else {
    argack.push(jo);
  }
};

retack.eval = function () {
  while (true) {
    if (retack.is_empty()) {
      return;
    }
    var retack_point = retack.pop();
    var jo = retack_point.get_current_jo();
    if (!retack_point.at_tail_position()) {
      retack_point.next();
      retack.push(retack_point);
    }
    retack.eval_dispatch(jo, retack_point);
  }
};

Array.prototype.push_to_retack = function () {
  retack.push(new RETACK_POINT(this));
};

Array.prototype.eval = function () {
  retack.push(new RETACK_POINT(this));
  retack.eval();
};

{
  var add = function add(a, b) {
    return a + b;
  };

  var testing_jojo = { jojo: [1, 2, 3] };

  var my_object = {
    k1: 'value k1 of my_object'
  };

  [1, 2, 3, add, add, { jojo: [1, 2, 3] }, add, add, testing_jojo, add, add, my_object, 'k1', send].eval();

  asr(argack.pop() === my_object.k1);
  asr(argack.pop() === 6);
  asr(argack.pop() === 6);
  asr(argack.pop() === 6);

  asr(argack.cursor === 0);
}

{
  var swap = { majo: [[v('a'), v('b')], [v('b'), v('a')]] };

  [1, 2, swap].eval();
  asr(argack.pop() === 1);
  asr(argack.pop() === 2);

  var testing_majo = { majo: [[1, v('a'), 3], ['1_3', v('a')], [4, v('a'), 6], ['4_6', v('a')], [7, v('a'), 9, { array: [v('a')] }], ['7_9', v('a')]] };

  [1, 2, 3, testing_majo].eval();
  asr(argack.pop() === 2);
  asr(argack.pop() === '1_3');

  [4, '555', 6, testing_majo].eval();
  asr(argack.pop() === '555');
  asr(argack.pop() === '4_6');

  [7, { structured_data: [8, 8, 8] }, 9, { array: [{ structured_data: [8, 8, 8] }] }, testing_majo].push_to_retack();
  retack.eval();

  asr(retack.unify_helper_equal(argack.pop(), { structured_data: [8, 8, 8] }));

  asr(argack.pop() === '7_9');

  asr(argack.cursor === 0);
}

function string_to_sum(string) {
  var sum = 0;
  var max_step = 10;
  for (var i = 0; i < string.length; i++) {
    sum = sum + string.codePointAt(i) * (2 << Math.min(i, max_step));
  }
  return sum;
}

var tag_hash_table = new HASH_TABLE(
// prime table size
997,
// key_equal
function (key1, key2) {
  return key1 === key2;
},
// hash
function (key, counter) {
  return (counter + string_to_sum(key)) % 997;
});

{
  var index = tag_hash_table.key_to_index("testkey0");
  var key = tag_hash_table.index_to_entry(index).key;
  asr(key === "testkey0");

  // tag_hash_table.report();
}

function array_equal(a, b) {
  if (a === b) return true;
  if (a == null || b == null) return false;
  if (a.length != b.length) return false;
  for (var i = 0; i < a.length; ++i) {
    if (a[i] !== b[i]) return false;
  }
  return true;
}

Array.prototype.last = function () {
  return this[this.length - 1];
};

function array_remove_dup(orig_array) {
  var array = orig_array.map(function (element) {
    return element;
  });
  array.sort();
  var result = [];
  for (var i = 0; i < array.length; i++) {
    if (result.last() === array[i]) {} else {
      result.push(array[i]);
    }
  }
  return result;
}

function array_remove(array, value) {
  var result = [];
  var _iteratorNormalCompletion2 = true;
  var _didIteratorError2 = false;
  var _iteratorError2 = undefined;

  try {
    for (var _iterator2 = array[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
      var element = _step2.value;

      if (element === value) {
        // nothing
      } else {
          result.push(element);
        }
    }
  } catch (err) {
    _didIteratorError2 = true;
    _iteratorError2 = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion2 && _iterator2.return) {
        _iterator2.return();
      }
    } finally {
      if (_didIteratorError2) {
        throw _iteratorError2;
      }
    }
  }

  return result;
}

var name_hash_table = new HASH_TABLE(
// prime table size
997,
// key_equal
function (key1, key2) {
  return array_equal(array_remove_dup(key1), array_remove_dup(key2));
},
// hash
function (key, counter) {
  var sum = array_remove_dup(key).map(string_to_sum).reduce(function (sum, v) {
    return sum + v;
  });
  return (counter + sum) % 997;
});

{
  name("testkey0");
  name("testkey1");
  name("testkey2");
  name("testkey3");
  name("testkey4");
  name("testkey1:testkey2:testkey3");
  name(":::testkey1:testkey2:testkey3");
  name("testkey1:testkey2");
  name("testkey4:testkey4:testkey1");
  name("testkey4:testkey1");

  asr(get(name("k1")) === null);

  set(name("k1:k1:k1"), 1);
  asr(get(name("k1")) === 1);

  set(name("k1:k1:k1"), null);
  asr(get(name("k1")) === null);

  // name_hash_table.report();
}

function string_to_key(string) {
  return array_remove(string.split(":"), "");
}

function key_to_string(key) {
  var result = "";
  var _iteratorNormalCompletion3 = true;
  var _didIteratorError3 = false;
  var _iteratorError3 = undefined;

  try {
    for (var _iterator3 = key[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
      var element = _step3.value;

      result = result.concat(":", element);
    }
  } catch (err) {
    _didIteratorError3 = true;
    _iteratorError3 = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion3 && _iterator3.return) {
        _iterator3.return();
      }
    } finally {
      if (_didIteratorError3) {
        throw _iteratorError3;
      }
    }
  }

  return result.substring(1);
}

function name(string) {
  return name_hash_table.key_to_index(string_to_key(string));
}

function name_to_string(name) {
  return key_to_string(name_hash_table.index_to_entry(name).key);
}

function set(name, value) {
  name_hash_table.index_to_entry(name).value = value;
}

function get(name) {
  return name_hash_table.index_to_entry(name).value;
}

function G(typename, type, data) {
  this.typename = typename;
  this.type = type;
  this.data = data;
}

G.prototype = {};

function L(typename, type, data) {
  G.call(this, typename, type, data);
  this.determinacy = 0;
}

L.prototype = {};

var argument_stack = new STACK(1024, 0);

function A(typename, data) {
  this.typename = typename;
  this.data = data;
}

var return_stack = new STACK(1024, 0);

function R(array) {
  this.cursor = 0;
  this.array = array;
  this.local_variable_map = new Map();
}

R.prototype = {
  get_current_jo: function get_current_jo() {
    return this.array[this.cursor];
  },
  next: function next() {
    this.cursor = 1 + this.cursor;
  }
};

function JO(explainer, data) {
  this.explainer = explainer;
  this.data = data;
}

JO.prototype = {
  happy: function happy() {
    this.explainer(this.data);
  }
};

function primitive_function_explainer(name) {
  get(name).data.call();
};

function function_explainer(name) {
  return_stack.push(new R(get(name).data));
};

function variable_explainer(name) {
  argument_stack.push(new A(get(name).typename, get(name).data));
};

return_stack.eval = function () {
  try {
    while (true) {
      var r = return_stack.tos();
      var jo = r.get_current_jo();
      r.next();
      jo.happy();
    }
  } catch (string) {
    if (string === "bye") {
      return;
    } else {
      orz("- unknown exception:\n", "  ", string);
    }
  }
};

function space_char_p(char) {
  if (char === '\n') {
    return true;
  } else if (char === '\t') {
    return true;
  } else if (char === ' ') {
    return true;
  } else {
    return false;
  }
}

function braket_char_p(char) {
  if (char === '(') {
    return true;
  } else if (char === ')') {
    return true;
  } else if (char === '[') {
    return true;
  } else if (char === ']') {
    return true;
  } else if (char === '{') {
    return true;
  } else if (char === '}') {
    return true;
  } else {
    return false;
  }
}

function space_string_p(string) {
  var _iteratorNormalCompletion4 = true;
  var _didIteratorError4 = false;
  var _iteratorError4 = undefined;

  try {
    for (var _iterator4 = string[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
      var char = _step4.value;

      if (space_char_p(char)) {
        // do nothing
      } else {
          return false;
        }
    }
  } catch (err) {
    _didIteratorError4 = true;
    _iteratorError4 = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion4 && _iterator4.return) {
        _iterator4.return();
      }
    } finally {
      if (_didIteratorError4) {
        throw _iteratorError4;
      }
    }
  }

  return true;
}

function READING_BUFFER(name, string) {
  this.size = string.length;
  this.name = name;
  this.string = string;
  this.cursor = 0;
}

READING_BUFFER.prototype = {
  at_start: function at_start() {
    return this.cursor === 0;
  },
  at_end: function at_end() {
    return this.cursor === this.size;
  },
  read_char: function read_char() {
    if (this.at_end()) {
      cat("- read_char fail\n", "  at the end of the READING_BUFFER", this.name);
    } else {
      this.cursor = 1 + this.cursor;
      return this.string.substring(this.cursor - 1, this.cursor);
    }
  },
  back_char: function back_char() {
    if (this.at_start()) {
      cat("- back_char fail\n", "  at the start of the READING_BUFFER:", this.name);
    } else {
      this.cursor = this.cursor - 1;
    }
  },
  to_next_non_space: function to_next_non_space() {
    if (this.at_end()) {
      // do nothing
    } else {
        var char = this.read_char();
        if (space_string_p(char)) {
          this.to_next_non_space();
        } else {
          this.back_char();
        }
      }
  },
  to_next_space: function to_next_space() {
    if (this.at_end()) {
      // do nothing
    } else {
        var char = this.read_char();
        if (space_string_p(char)) {
          this.back_char();
        } else {
          this.to_next_space();
        }
      }
  },
  read_word: function read_word() {
    this.to_next_non_space();
    if (this.at_end()) {
      cat("- read_word fail\n", "  at the end of the READING_BUFFER:", this.name);
    } else {
      var char = this.read_char();
      if (braket_char_p(char)) {
        return char;
      } else {
        var word_start = this.cursor - 1;
        this.to_next_space();
        return this.string.substring(word_start, this.cursor);
      }
    }
  }
};

{
  var k1 = new READING_BUFFER("k1", "一二三 \n 三二一");
  asr(k1.read_word() === "一二三");
  asr(k1.read_word() === "三二一");
  // k1.read_word();
}

var current_reading_buffer = null;

function def(name_string, typename_string, type, data) {
  set(name(name_string), new G(name(typename_string), type, data));
}

def("end", "->", null, function () {
  return_stack.pop();
});

def("bye", "->", null, function () {
  console.log("bye bye ^-^/");
  throw "bye";
});

def("dup", "->", null, function () {
  var a = argument_stack.tos();
  argument_stack.push(a);
});

def("mul", "->", null, function () {
  var a1 = argument_stack.pop();
  var a2 = argument_stack.pop();
  argument_stack.push(new A(name("integer"), a1.data * a2.data));
});

def("simple-wirte", "->", null, function () {
  var a = argument_stack.pop();
  console.log(name_to_string(a.typename), a.data);
});

def("little-test-number", "integer", "integer", 4);

def("square", "->", null, [new JO(primitive_function_explainer, name("dup")), new JO(primitive_function_explainer, name("mul")), new JO(primitive_function_explainer, name("end"))]);

def("little-test", "->", null, [new JO(variable_explainer, name("little-test-number")), new JO(function_explainer, name("square")), new JO(primitive_function_explainer, name("simple-wirte")), new JO(primitive_function_explainer, name("bye"))]);

function begin() {
  return_stack.push(new R([new JO(function_explainer, name("little-test")), new JO(primitive_function_explainer, name("end"))]));
  return_stack.eval();
}

{
  begin();
}

exports.default = {
  tag_hash_table: tag_hash_table, name_hash_table: name_hash_table, name: name, set: set, get: get
};