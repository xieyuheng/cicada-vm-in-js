    'use strict';

    //// import ><><>< from '><><><';
    let isin = {};

    isin.node = function () {
      return (typeof module !== 'undefined');
    };

    isin.browser = function () {
      return (typeof window !== 'undefined');
    };
    let is = {};

    is.function = function (value) {
      return typeof value === 'function';
    };

    is.undefined = function (value) {
      return value === undefined;
    };

    is.array = function (value) {
      return Array.isArray(value);
    };

    is.object = function (value) {
      return (value instanceof Object) && !is.array(value);
    };

    is.atom = function (value) {
      return !(is.array(value) || is.object(value));
    };

    is.string = function (value) {
      return typeof value === 'string';
    };
    let cat_indentation = 0;

    function cat_get_indentation_array () {
      let result = [];
      for (let i = 0; i < cat_indentation; i++) {
        result.push(" ");
      }
      return result;
    }

    function cat () {
      let argument_array = [];
      for (let argument of arguments) {
        argument_array.push(argument);
      }
      if (isin.node()) {
        console.log.apply(
          console,
          cat_get_indentation_array()
            .concat(argument_array));
      } else if (isin.browser()) {
        console.log.apply(console, argument_array);
      } else {
        console.log.apply(
          console,
          cat_get_indentation_array()
            .concat(argument_array));
      }
    }

    function call_with_indentation (fun) {
      if (isin.node()) {
        cat_indentation = 1 + cat_indentation;
        fun.call(this);
        cat_indentation = cat_indentation - 1;
      } else if (isin.browser()) {
        console.group();
        fun.call(this);
        console.groupEnd();
      } else {
        cat_indentation = 1 + cat_indentation;
        fun.call(this);
        cat_indentation = cat_indentation - 1;
      }
    }

    function incat () {
      call_with_indentation (() => {
        cat.apply(this, arguments);
      });
    }

    // {
    //   cat("k1", "k2", "k3");
    //   incat("k1", "k2", "k3");
    //   call_with_indentation(() => {
    //     call_with_indentation(() => cat("k1", "k2", "k3"));
    //     call_with_indentation(() => incat("k1", "k2", "k3"));
    //   });

    //   cat("k1", "k2", "k3");
    //   incat("k1", "k2", "k3");
    //   call_with_indentation(() => {
    //     call_with_indentation(() => cat("k1", "k2", "k3"));
    //     call_with_indentation(() => incat("k1", "k2", "k3"));
    //   });
    // }
    function orz () {
      cat.apply(this, arguments);
      console.assert(false);
    }

    // {
    //   orz("k1", "k2", "k3");
    // }
    function asr () {
      console.assert.apply(console, arguments);
    }
    function BUFFER (size, init_value) {
      this.size = size;
      this.array = new Array(size);
      this.array.fill(init_value);
      this.cursor = 0;
    }

    BUFFER.prototype = {
      set: function (index, value) {
        this.array[index] = value;
      },
      get: function (index) {
        return this.array[index];
      },
      add_cursor: function (value) {
        this.cursor = this.cursor + value;
      },
      allocate: function (size) {
        const return_address = this.cursor;
        this.add_cursor(size);
        return return_address;
      }
    };
    function STACK (size, init_value) {
      BUFFER.call(this, size, init_value);
    }

    STACK.prototype = Object.create(BUFFER.prototype);

    STACK.prototype.push = function (value) {
      this.set(this.cursor, value);
      this.add_cursor(1);
    };

    STACK.prototype.pop = function () {
      this.add_cursor(-1);
      return this.get(this.cursor);
    };

    STACK.prototype.tos = function () {
      return this.get(this.cursor - 1);
    };

    STACK.prototype.array_push = function (array) {
      for (let value of array) {
        this.push(value);
      }
    };

    STACK.prototype.n_pop = function (n) {
      let array = [];
      while (n > 0) {
        array.push(this.pop());
        n = n - 1;
      }
      return array.reverse();
    };

    STACK.prototype.n_tos = function (n) {
      let the_n = n;
      let array = [];
      while (n > 0) {
        array.push(this.pop());
        n = n - 1;
      }
      this.cursor = the_n + this.cursor;
      return array.reverse();
    };

    STACK.prototype.drop = function () {
      this.add_cursor(-1);
    };

    STACK.prototype.n_drop = function (n) {
      this.add_cursor(-n);
    };

    STACK.prototype.is_empty = function () {
      return this.cursor === 0;
    };

    STACK.prototype.print = function () {
      let index = 0;
      let arg_list = [];
      while (index < this.cursor) {
        arg_list.push(this.get(index));
        index = 1 + index;
      }
      arg_list.unshift('  *', this.cursor, '*  --');
      arg_list.push('--');
      console.log.apply(console, arg_list);
    };

    {
      const stack = new STACK(1024, 0);

      stack.push(666);
      asr(stack.pop() === 666);

      stack.array_push([0,1,2]);
      let array = stack.n_pop(3);
      asr(array[0] === 0);
      asr(array[1] === 1);
      asr(array[2] === 2);
    }
    function HASH_TABLE_ENTRY (index) {
      this.index = index;
      this.key = null;
      this.value = null;
      this.orbit_length = 0;
      this.orbiton = 0;
    }

    HASH_TABLE_ENTRY.prototype = {
      occured: function () {
        return this.key !== null;
      },

      used: function () {
        return this.value !== null;
      },

      no_collision: function () {
        return this.index === this.orbiton;
      }
    };

    function HASH_TABLE (size,
                         key_equal,
                         hash) {
      this.size = size;
      this.key_equal = key_equal;
      this.hash = hash;
      this.array = new Array(size);
      this.counter = 0;
      for (let i = 0; i < size ; i++) {
        this.array[i] = new HASH_TABLE_ENTRY(i);
      };
    }

    HASH_TABLE.prototype = {
      insert: function (key) {
        // key -> index
        //     -> null -- denotes the hash_table is filled
        let orbit_index = this.hash(key, 0);
        let counter = 0;
        while (true) {
          let index = this.hash(key, counter);
          let entry = this.index_to_entry(index);
          if (!entry.occured()) {
            entry.key = key;
            entry.orbiton = orbit_index;
            let orbit_entry = this.index_to_entry(orbit_index);
            orbit_entry.orbit_length = 1 + counter;
            this.counter = 1 + this.counter;
            return index;
          } else if (this.key_equal(key, entry.key)) {
            return index;
          } else if (counter === this.size) {
            return null;
          } else {
            counter = 1 + counter;
          }
        }
      },

      search: function (key) {
        // key -> index
        //     -> null -- denotes key not occured
        let counter = 0;
        while (true) {
          let index = this.hash(key, counter);
          let entry = this.index_to_entry(index);
          if (!entry.occured()) {
            return null;
          } else if (this.key_equal(key, entry.key)) {
            return index;
          } else if (counter === this.size) {
            return null;
          } else {
            counter = 1 + counter;
          }
        }
      },

      key_to_index: function (key) {
        let index = this.insert(key);
        if (index !== null) {
          return index;
        } else {
          console.log("hash_table is filled");
          throw "hash_table is filled";
        }
      },

      index_to_entry: function (index) {
        return this.array[index];
      },

      key_to_entry: function (key) {
        return index_to_entry(key_to_index(key));
      },

      report_orbit: function (index, counter) {
        let entry = this.index_to_entry(index);
        while (true) {
          if (counter >= entry.orbit_length) {
            return;
          } else {
            let key = entry.key;
            let next_index = this.hash(key, counter);
            let next_entry = this.index_to_entry(next_index);
            if (index === next_entry.orbiton) {
              console.log("  - {", next_index, "} ",
                          next_entry.key); }
            counter = 1 + counter;
          }
        }
      },

      report: function () {
        console.log("\n");
        console.log("- hash_table-table report_used");
        let index = 0;
        while (true) {
          let entry = this.index_to_entry(index);
          if (index === this.size) {
            break;
          } else {
            if (entry.occured() &&
                entry.no_collision()) {
              console.log("  - {", index, "} ",
                          entry.key, " # ",
                          entry.orbit_length);
              if (entry.used()) {
                console.log("      ", entry.value);
              }
              this.report_orbit(index, 1);
            }
            index = 1 + index;
          }
        }
        console.log("\n");
        console.log("- used : ", this.counter);
        console.log("- free : ", this.size - this.counter);
      }
    };
    function string_to_sum (string) {
      let sum = 0;
      let max_step = 10;
      for (let i = 0; i < string.length; i++) {
        sum = sum +
          string.codePointAt(i) *
          (2 << Math.min(i, max_step));
      }
      return sum;
    }
    const tag_hash_table = new HASH_TABLE(
      // prime table size
      997,
      // key_equal
      (key1, key2) => {
        return key1 === key2;
      },
      // hash
      (key, counter) => {
        return (counter + string_to_sum(key)) % 997;
      }
    );

    {
      let index = tag_hash_table.key_to_index("testkey0");
      let key = tag_hash_table.index_to_entry(index).key;
      asr(key === "testkey0");

      // tag_hash_table.report();
    }
    function array_equal (a, b) {
      if (a === b) return true;
      if (a == null || b == null) return false;
      if (a.length != b.length) return false;
      for (var i = 0; i < a.length; ++i) {
        if (a[i] !== b[i]) return false;
      }
      return true;
    }

    Array.prototype.last = function () {
      return this[this.length - 1];
    };

    function array_remove_dup (orig_array) {
      const array = orig_array.map((element) => {return element;});
      array.sort();
      let result = [];
      for (let i = 0; i < array.length; i++) {
        if (result.last() === array[i]) {
        } else {
          result.push(array[i]);
        }
      }
      return result;
    }

    function array_remove (array, value) {
      let result = [];
      for (let element of array) {
        if (element === value) {
          // nothing
        } else {
          result.push(element);
        }
      }
      return result;
    }
    const name_hash_table = new HASH_TABLE(
      // prime table size
      997,
      // key_equal
      (key1, key2) => {
        return array_equal(array_remove_dup(key1),
                           array_remove_dup(key2));
      },
      // hash
      (key, counter) => {
        let sum =
            array_remove_dup(key)
            .map(string_to_sum)
            .reduce((sum, v) => {return sum + v;});
        return (counter + sum) % 997;
      }
    );

    {
      name("testkey0");
      name("testkey1");
      name("testkey2");
      name("testkey3");
      name("testkey4");
      name("testkey1:testkey2:testkey3");
      name(":::testkey1:testkey2:testkey3");
      name("testkey1:testkey2");
      name("testkey4:testkey4:testkey1");
      name("testkey4:testkey1");

      asr(get(name("k1")) === null);

      set(name("k1:k1:k1"), 1);
      asr(get(name("k1")) === 1);

      set(name("k1:k1:k1"), null);
      asr(get(name("k1")) === null);

      // name_hash_table.report();
    }
    function string_to_key (string) {
      return array_remove(string.split(":"), "");
    }

    function key_to_string (key) {
      let result = "";
      for (let element of key) {
        result = result.concat(":", element);
      }
      return result.substring(1);
    }

    function name (string) {
      return name_hash_table.key_to_index(string_to_key(string));
    }

    function name_to_string (name) {
      return key_to_string(name_hash_table.index_to_entry(name).key);
    }
    function set (name, value) {
      name_hash_table.index_to_entry(name).value = value;
    }

    function get (name) {
      return name_hash_table.index_to_entry(name).value;
    }
    const argument_stack = new STACK(1024, 0);
    const return_stack = new STACK(1024, 0);
    function R (jojo, mode) {
      this.jojo = jojo;
      if (mode === undefined) {
        this.mode = "compute";
      } else {
        this.mode = mode;
      }
      this.cursor = 0;
      this.local_variable_map = new Map();
    }

    R.prototype = {
      get_current_jo: function () {
        return this.jojo[this.cursor];
      },
      next: function () {
        this.cursor = 1 + this.cursor;
      }
    };
    return_stack.eval = function () {
      try {
        while (true) {
          let r = return_stack.tos();
          let jo = r.get_current_jo();
          r.next();
          if (jo[0] === name("name")) {
            //// debug
            // cat(name_to_string(jo[1]));
            let g = get(jo[1]);
            g[r.mode](g);
          } else if (jo[0] === name("local-variable")) {
            // ><><><
          } else {
            if (r.mode === "compute") {
              argument_stack.push(jo);
            } else {
              // check
            }
          }
        }
      } catch (string) {
        if (string === "bye") {
          return;
        } else {
          orz("- unknown exception:\n",
              "  ", string);
        }
      }
    };
    let i_apply_primitive_function_from_name = {
      compute: function (g) {
        g.data[1].call();
      },
      check: function (g) {
        // ><><><
      }
    };
    let i_apply_function_from_name = {
      compute: function (g) {
        return_stack.push(
          new R (g.data[1]));
      },
      check: function (g) {
        // ><><><
      }
    };
    let i_push_data_from_name = {
      compute: function (g) {
        argument_stack.push(g.data);
      },
      check: function (g) {
        // ><><><
      }
    };
    let i_push_data = {
      compute: function (data) {
        argument_stack.push(data);
      },
      check: function (data) {
        // ><><><
      }
    };
    let i_push_data_from_local_variable = {
      compute: function (local_variable) {

      },
      check: function (local_variable) {

      }
    };
    let i_unify_antecedent = {
      compute: function (antecedent) {

      },
      check: function (antecedent) {

      }
    };
    let i_apply_succedent = {
      compute: function (succedent) {

      },
      check: function (succedent) {

      }
    };
    let i_apply_sequent_array = {
      compute: function (sequent_array) {

      },
      check: function (sequent_array) {

      }
    };
    function array_to_jojo (array) {
      let result_jojo = [];
      for (let element of array) {
        if (is.string(element)) {
          result_jojo.push([name("name"),
                            name(element)]);
        } else {
          orz("- array_to_jojo",
              "  unknown type of element:", element);
        }
      }
      return result_jojo;
    }
    function defjojo (name_string, type, array) {
      let result_type = []; // ><><>< bo be handled
      set (
        name(name_string),
        { type: [ name("type"),
                  result_type ],
          data: [ name("->"),
                  array_to_jojo(array) ],
          check: i_apply_function_from_name.check,
          compute: i_apply_function_from_name.compute
        }
      );
    }
    function defprim (name_string, type, prim) {
      let result_type = []; // ><><>< bo be handled
      set (
        name(name_string),
        { type: [ name("type"),
                  result_type ],
          data: [ name("->"),
                  prim ],
          check: i_apply_primitive_function_from_name.check,
          compute: i_apply_primitive_function_from_name.compute
        }
      );
    }
    function defdata (name_string, type, data) {
      let result_type = []; // ><><>< bo be handled
      set (
        name(name_string),
        { type: [ name("type"),
                  result_type ],
          data: data,
          check: i_push_data_from_name.check,
          compute: i_push_data_from_name.compute
        }
      );
    }
    function evajojo (array) {
      return_stack.push(new R (array_to_jojo(array)));
      return_stack.eval();
    }
    function push(value) { argument_stack.push(value); }
    function array_push(array) { argument_stack.array_push(array); }

    function pop() { return argument_stack.pop(); }
    function n_pop(n) { return argument_stack.n_pop(n); }

    function tos() { return argument_stack.tos(); }
    function n_tos(n) { return argument_stack.n_tos(n); }

    function drop() { argument_stack.drop(); }
    function n_drop(n) { argument_stack.n_drop(); }
    defprim (
      "end",
      [],
      () => {
        return_stack.pop();
      }
    );

    defprim (
      "bye",
      [],
      () => {
        console.log("bye bye ^-^/");
        throw "bye";
      }
    );
    defprim (
      "dup",
      [],
      () => {
        let a = tos();
        push(a);
      }
    );
    defprim (
      "mul",
      [],
      () => {
        let a1 = pop();
        let a2 = pop();
        push(
          [name("integer"),
           a1[1] * a2[1]]);
      }
    );
    defprim (
      "simple-wirte",
      [],
      () => {
        let a = pop();
        console.log(name_to_string(a[0]),
                    a[1]);
      }
    );

      let argument_stack_snapshot = 0;

      function set_argument_stack_snapshot () {
        argument_stack_snapshot = argument_stack.cursor;
      }
      function get_snapshot_different_number () {
        return (argument_stack.cursor - argument_stack_snapshot);
      }
      function jo_equal (jo1, jo2) {
        return ;
      }
      function jo_unify (jo1, jo2) {

      }
      function n_unify (n) {
        let jo1 = [name("->"), n_pop(n)];
        let jo2 = [name("->"), n_tos(n)];
        return jo_unify(jo1, jo2);
      }
      defprim (
        "+",
        [],
        () => {
          set_argument_stack_snapshot();
        }
      );
      defprim (
        "->",
        () => {
          //   ><><><
        },
        () => {
          let n = get_snapshot_different_number();
          let r = return_stack.tos();
          let jo = r.get_current_jo();
          r.next();
          if (jo[0] === name("->")) {
            orz("- ->\n",
                "  what follows the -> should be a ->\n",
                "  instead of :", jo);
            return;
          }
          let success = n_unify(n);
          if (success) {
            r.cursor = 0;
            r.jojo = jo;
          } else {
            r.next();
          }
        }
      );
    function space_char_p (char) {
           if (char === '\n') {return true;}
      else if (char === '\t') {return true;}
      else if (char === ' ') {return true;}
      else {return false;}
    }
    function braket_char_p (char) {
           if (char === '(') {return true;}
      else if (char === ')') {return true;}
      else if (char === '[') {return true;}
      else if (char === ']') {return true;}
      else if (char === '{') {return true;}
      else if (char === '}') {return true;}
      else {return false;}
    }
    function double_quote_char_p (char) {
      if (char === '"') {return true;}
      else {return false;}
    }
    function number_char_p (char) {
      return ((char.codePointAt(0) >= "0".codePointAt(0)) &&
              (char.codePointAt(0) <= "9".codePointAt(0)));
    }
    function space_string_p (string) {
      for (var char of string) {
        if (space_char_p(char)) {
          // do nothing
        } else {
          return false;
        }
      }
      return true;
    }
    function number_string_p (string) {
      if (string.length === 0) { return false; }
      else {
        for (let char of string) {
          if (!number_char_p(char)) {
            return false;
          }
        }
        return true;
      }
    }

    {
      asr(number_string_p("123") === true);
      asr(number_string_p("-123") === false);
    }
    function integer_string_p (string) {
           if (string.length === 0) { return false; }
      else if (string.length === 1) { return number_char_p(string); }
      else if (string.substring(0, 1) === "-") {
        return number_string_p(string.substring(1));
      } else {
        return number_string_p(string);
      }
    }

    {
      asr(integer_string_p("123") === true);
      asr(integer_string_p("-123") === true);
    }
    function string_to_vm_integer (string) {
      return [name("integer"), parseInt(string, 10)];
    }
    let keyword_processer_stack = new STACK(1024, 0);
    function process_word (word) {
      let cursor = keyword_processer_stack.cursor;
      while (cursor !== 0) {
        cursor = cursor - 1;
        let keyword_processer =
            keyword_processer_stack.get(cursor);
        if (keyword_processer.predicate(word)) {
          return keyword_processer.syntax_function(word);
        }
      }
      return null;
    }
    let keyword_record = new Map();
    function syntax_round_bra () {
      let word = current_reading_buffer.read_word();
      let result = process_word(word);
      if (result !== null) {
        return result;
      } else {
        orz("- syntax_round_bra\n",
            "  do not know how to process word:", word, "\n",
            "  cursor:", current_reading_buffer.cursor);
      }
    }
    // { [READING_BUFFER] -> [set name_hash_table] }
    function syntax_defjojo () {
      let name_string = current_reading_buffer.read_word();
      let result_type = []; // ><><>< bo be handled
      let result = process_word(current_reading_buffer.read_word());
      if (result !== null) {
        set (
          name(name_string),
          { type: [ name("type"), result_type ],
            data: result,
            check: i_apply_function_from_name.check,
            compute: i_apply_function_from_name.compute
          }
        );
      } else {
        orz("- syntax_defjojo\n",
            "  do not know how to process word:", word, "\n",
            "  cursor:", current_reading_buffer.cursor);
      }
    }
    // { [READING_BUFFER] -> [set name_hash_table] }
    function syntax_defdata () {
      let name_string = current_reading_buffer.read_word();
      let result_type = []; // ><><>< bo be handled
      let result = process_word(current_reading_buffer.read_word());
      if (result !== null) {
        set (
          name(name_string),
          { type: [ name("type"), result_type ],
            data: result,
            check: i_push_data_from_name.check,
            compute: i_push_data_from_name.compute
          }
        );
      } else {
        orz("- syntax_defdata\n",
            "  do not know how to process word:", word, "\n",
            "  cursor:", current_reading_buffer.cursor);
      }
    }
    // { [READING_BUFFER] -> data }
    function syntax_jojo () {
      let array = [];
      while (true) {
        let word = current_reading_buffer.read_word();
        if (word === "]") {
          return [name("->"), array];
        } else {
          let result = process_word(word);
          if (result !== null) {
            array.push(result);
          } else {
            array.push([name("name"), name(word)]);
          }
        }
      }
    }
    // keyword_record.set("(", syntax_round_bra);
    keyword_record.set("defjojo", syntax_defjojo);
    keyword_record.set("defdata", syntax_defdata);
    keyword_record.set("[", syntax_jojo);
    keyword_processer_stack.push ({
      predicate: (word) => {
        return keyword_record.has(word);
      },
      syntax_function: (word) => {
        return keyword_record.get(word).call();
      }
    });

    keyword_processer_stack.push ({
      predicate: integer_string_p,
      syntax_function: string_to_vm_integer
    });
    function READING_BUFFER (name, string) {
      this.size = string.length;
      this.name = name;
      this.string = string;
      this.cursor = 0;
    }

    READING_BUFFER.prototype = {
      at_start: function () {
        return this.cursor === 0;
      },

      at_end: function () {
        return this.cursor === this.size;
      },

      space: function () {
        return space_string_p(
          this.string.substring(this.cursor));
      },

      read_char: function () {
        if (this.at_end()) {
          cat("- read_char fail\n",
              "  at the end of the READING_BUFFER", this.name);
        }
        else {
          this.cursor = 1 + this.cursor;
          return this.string
            .substring(this.cursor - 1, this.cursor);
        }
      },

      back_char: function () {
        if (this.at_start()) {
          cat("- back_char fail\n",
              "  at the start of the READING_BUFFER:", this.name);
        } else {
          this.cursor = this.cursor - 1;
        }
      },

      // recursive helper function
      to_word_start: function () {
        if (this.at_end()) {
          // do nothing
        } else {
          let char = this.read_char();
          if (space_string_p(char)) {
            this.to_word_start();
          } else {
            this.back_char();
          }
        }
      },

      // recursive helper function
      to_word_end: function () {
        if (this.at_end()) {
          // do nothing
        } else {
          let char = this.read_char();
          if (space_string_p(char)) {
            this.back_char();
          } else if (braket_char_p(char)) {
            this.back_char();
          } else if (double_quote_char_p(char)) {
            this.back_char();
          } else {
            this.to_word_end();
          }
        }
      },

      read_word: function () {
        this.to_word_start();
        if (this.space()) {
          cat("- read_word fail\n",
              "  only space left in the READING_BUFFER:", this.name);
        } else {
          let char = this.read_char();
          if (braket_char_p(char)) {
            return char;
          } else if (double_quote_char_p(char)) {
            return char;
          } else {
            let word_start = this.cursor - 1;
            this.to_word_end();
            return this.string
              .substring(word_start, this.cursor);
          }
        }
      }
    };

    {
      let k1 = new READING_BUFFER ("k1", "一二三 \n 三二一");
      asr(k1.read_word() === "一二三");
      asr(k1.read_word() === "三二一");
      // k1.read_word();

      let k2 = new READING_BUFFER ("k2", "(define lambda)");
      asr(k2.read_word() === "(");
      asr(k2.read_word() === "define");
      asr(k2.read_word() === "lambda");
      asr(k2.read_word() === ")");
    }
    const fs = require("fs");

    function file_to_reading_buffer (file_name) {
      try {
        return new READING_BUFFER(
          file_name,
          fs.readFileSync(file_name).toString());
      } catch (error) {
        orz(error);
      }
    }

    {
      // cat(file_to_reading_buffer("README"));
    }
    let current_reading_buffer = null;
    function assembler (file_name) {
      current_reading_buffer = file_to_reading_buffer(file_name);
      console.log("- assembling:", file_name);
      while (!current_reading_buffer.space()) {
        let word = current_reading_buffer.read_word();
        process_word(word);
      }
    }
    // defdata (
    //   "little-test-number",
    //   [],
    //   [ name("integer"),
    //     4 ]
    // );

    // defjojo (
    //   "square",
    //   [],
    //   [ "dup",
    //     "mul",
    //     "end",
    //   ]
    // );

    // defjojo (
    //   "little-test",
    //   [],
    //   [ "little-test-number",
    //     "square",
    //     "simple-wirte",
    //     "bye",
    //   ]
    // );

    // evajojo (
    //   [ "little-test",
    //     "end",
    //   ]
    // );
    assembler("test/test.cn");

    evajojo (
      [ "little-test",
        "end",
      ]
    );
    // export default {
    //   tag_hash_table, name_hash_table, name, set, get,
    // };
