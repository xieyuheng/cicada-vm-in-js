"use strict";

let gulp = require("gulp");
let execSync = require("child_process").execSync;

function run () {
  let result = "";
  for (let string of arguments) {
    result = result.concat(" ", string);
  }
  console.log(execSync(result).toString());
}

const fs = require("fs");
const path = require("path");

function tangle (input_file_name) {
  let output_file_name = null;
  let line_array =
      fs.readFileSync(input_file_name)
      .toString()
      .split("\n");
  let cursor = 0;
  let result_array = [];
  while (cursor < line_array.length) {
    let line = line_array[cursor];
    cursor = 1 + cursor;
    if (line.match(/#\+PROPERTY: tangle .*/)) {
      output_file_name = path.join(
        __dirname,
        path.dirname(input_file_name),
        line
          .match(/#\+PROPERTY: tangle .*/)[0]
          .match(/\b[A-Za-z0-9_\-]*\.js/)[0]);
    } else if (line.match(/#\+begin_src.*/)) {
      while (cursor < line_array.length) {
        let line = line_array[cursor];
        cursor = 1 + cursor;
        if (line.match(/#\+end_src.*/)) {
          break;
        }  else {
          result_array.push(line);
        }
      };
    } else {
      // do nothing
    }
  }
  console.log("- tangle to :", output_file_name);
  fs.writeFileSync(
    output_file_name,
    line_array_to_string(result_array));
}

function line_array_to_string (line_array) {
  let result_string = "";
  for (let line of line_array) {
    result_string = result_string.concat(line, "\n");
  }
  return result_string;
}

let org_array = [
  "src/cicada-vm.org"
];

gulp.task("default", () => {
  for (let task_name
       of Object.keys(gulp.tasks).slice(1)) {
    console.log("-", task_name);
  }
});

gulp.task("build", () => {
  // run("rm -rf lib && babel src --out-dir lib");
});

gulp.task("tangle", () => {
  for (let org of org_array) {
    tangle(org);
  }
});

gulp.task("bundle", () => {
  // run("browserify lib/cicada-vm.js -o lib/bundle.js");
});

gulp.task("run", () => {
  run("node src/cicada-vm.js");
});

gulp.task("dev", [ "tangle", "build", "run" ], () => {
});

gulp.task("clean", () => {
  console.log("cleaning *~");
  run("find . -name '*~' -delete");
});
